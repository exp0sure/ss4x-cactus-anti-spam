<?php
/**
 *  Adds Honeypot and Timestamp Spam Protection for
 *  - Userforms Extension (if installed)
 *  - Comments Extension (if installed)
 */
namespace {
    use Silverstripe\Forms\FormField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HiddenField;

    class HoneypotSpamProtectorField extends FormField
    {

        const FORM_FIELD_NAME = 'usernamecontact';              // name of honeypot field
        const FORM_FIELD_NAME_TIMER = 'usernamecontacttimer';   // name of timestamp field
        const FORM_FIELD_LABEL = 'Captcha';

        public function Field($properties = array()) {
            Requirements::css('anti-spam/css/silverstripe_ext_ufs.css');

            $honeypot = CompositeField::create(
                new TextField($this->name.self::FORM_FIELD_NAME, self::FORM_FIELD_LABEL, ''),
                new HiddenField($this->name.self::FORM_FIELD_NAME_TIMER, null, time())
            );
            $honeypot->addExtraClass('usernamecontact');
            return $honeypot;
        }

        public function FieldHolder($properties = array()) {
            return $this->Field($properties);
        }

        /**
         *  Validates if:
         *  - Timestamp field is 0
         *  - Form submission happened > 3 seconds after form page load
         *  - Form submission happened < 1 day after form page load
         *  - Honeypot input field is empty
         */
        public function validate($validator) {
            $Request = $this->getForm()->getRequest();
            $submittedFieldContent = $Request->requestVar($this->name.self::FORM_FIELD_NAME);
            $timerContent = intval($Request->requestVar($this->name.self::FORM_FIELD_NAME_TIMER));

            if (
                $submittedFieldContent !== ''
                || $timerContent == 0
                || (time() - $timerContent) < 3
                || (time() - $timerContent) > 90000) {
                $validator->validationError(
                    $this->name,
                    'Sorry we are unable to process this. Please send us an email with your request.',          // not displayed on website?
                    "validation",
                    false
                );

                return false;
            }

            return true;
        }

        public function Type() {
            return '';
        }

    }
}